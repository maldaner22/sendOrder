package com.example.thiagoweber.sendorder;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    int num = 1, coffee = 3, choco, chantilly;
    double price, dolar = 3.256;
    boolean namecheck = false;
    String resumeOrder, showName, showTopping, showQuantity, showPrice;
    AlertDialog alert;
    String email[] = {"thiago.maldaner@digitaldesk.com.br"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void clicksend(View v) {
        reset();
        name();
        if (namecheck) {
            check();
            quantity();
            total();
            pedido();
            //message_box();

            Intent resumeIntent = new Intent();
            resumeIntent.setAction(Intent.ACTION_SEND);
            resumeIntent.putExtra(Intent.EXTRA_EMAIL, email);
            resumeIntent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.titleEmail));
            resumeIntent.putExtra(Intent.EXTRA_TEXT, resumeOrder);
            resumeIntent.setType("text/plain");
            startActivity(resumeIntent);
        }
    }

    public void name() {
        EditText editText = (EditText) findViewById(R.id.editText);
        String message = editText.getText().toString();

        if ("".equals(message)) {
            Toast.makeText(MainActivity.this, R.string.typeName, Toast.LENGTH_SHORT).show();
        } else {
            showName = (getResources().getString(R.string.resumeName) + " " + message);
            namecheck = true;
        }
    }

    public void check() {
        CheckBox checkChocolate = (CheckBox) findViewById(R.id.chocolate);
        CheckBox checkChantilly = (CheckBox) findViewById(R.id.chantilly);

        if ((checkChocolate).isChecked()) {
            showTopping = (getResources().getString(R.string.resumeTopping) + " " + getResources().getString(R.string.chocolate));
            choco = 1;
        }
        if ((checkChantilly).isChecked()) {
            showTopping = (getResources().getString(R.string.resumeTopping) + " " + getResources().getString(R.string.chantilly));
            chantilly = 2;
        }
        if ((checkChantilly).isChecked() && (checkChocolate).isChecked()) {
            showTopping = (getResources().getString(R.string.resumeTopping) + " " + getResources().getString(R.string.chocolate) + ", " + getResources().getString(R.string.chantilly));
            choco = 1;
            chantilly = 2;
        }
        if (!(checkChantilly).isChecked() && !(checkChocolate).isChecked()) {
            showTopping = (getResources().getString(R.string.noTopping));
            choco = 0;
            chantilly = 0;
        }
    }

    public void quantity() {
        showQuantity = (getResources().getString(R.string.resumeQuantity) + " " + num);
    }

    public void total() {
        if (Locale.getDefault().getLanguage().equals("en")) {
            price = ((coffee + choco + chantilly) * num) / dolar;
            showPrice = (getResources().getString(R.string.resumePrice) + " " + (String.format("US$ %.2f", price)));
        } else {
            price = (coffee + choco + chantilly) * num;
            showPrice = (getResources().getString(R.string.resumePrice) + " " + (String.format("R$ %.2f", price)));
        }
    }

    public void clickMinus(View a) {
        TextView number = (TextView) findViewById(R.id.number);
        String aux = number.getText().toString();
        num = Integer.parseInt(aux);
        if (num == 1) {
            Toast.makeText(MainActivity.this, R.string.minimum, Toast.LENGTH_SHORT).show();
        } else {
            num--;
            String show = String.valueOf(num);
            number.setText(show);
        }
    }

    public void clickPlus(View b) {
        TextView number = (TextView) findViewById(R.id.number);
        String aux = number.getText().toString();
        num = Integer.parseInt(aux);
        if (num == 100) {
            Toast.makeText(MainActivity.this, R.string.maximum, Toast.LENGTH_SHORT).show();
        } else {
            num++;
            String show = String.valueOf(num);
            number.setText(show);
        }
    }

    public void reset() {
        choco = 0;
        price = 0;
        chantilly = 0;
        namecheck = false;
    }

    public void pedido() {
        resumeOrder = (showName + "\n" +
                showTopping + "\n" +
                showQuantity + "\n" +
                showPrice);
    }

    /*public void message_box() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.resumeOrder));
        builder.setMessage(resumeOrder);
        alert = builder.create();
        alert.show();
    }*/
}